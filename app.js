const url = 'https://ayuda.descuartizadora.info/ollas'
fetch(url).then(response => response.json()).then(data => {
  data.reverse()
  data.forEach((ollacomun, index) => {
    var olla = $(`<div id="olla-${ollacomun.id}" class="olla"></div>`)
    if(ollacomun.nombre) olla.append(`<h2 class="nombre"><b><i>${ollacomun.nombre}</i></b></h2>`)
    direccion = ollacomun.direccion ? ollacomun.direccion + ', ' : ''
    olla.append(
      `<h4 class="direccion">${direccion}${ollacomun.comuna}</h4> 
      <p class="ciudad">${ollacomun.ciudad}, ${region(ollacomun.region)}</p>`
    )
    if(ollacomun.necesidades) olla.append(`<p><b><i>Qué hace falta:</i></b> ${ollacomun.necesidades}</p>`)
    if(ollacomun.instagram) olla.append(`<h4><b class="tag"><i>Instagram:</i></b> <a href="https://www.instagram.com/${ollacomun.instagram}" target="_blank">@${ollacomun.instagram}</a></h4>`)
    if(ollacomun.facebook) olla.append(`<h4><b class="tag"><i>Facebook:</i></b> <a href="${ollacomun.facebook}" target="_blank">${ollacomun.facebook}</h4>`)
    if(ollacomun.fono) olla.append(`<h4><b class="tag"><i>Fono:</i></b> <a href="tel:${ollacomun.fono}" target="_blank">${ollacomun.fono}</a></h4>`)
    if(ollacomun.email) olla.append(`<h4><b class="tag"><i>Email:</i></b> <a href="tel:${ollacomun.email}" target="_blank">${ollacomun.email}</a></h4>`)
    if(index < data.length - 1) olla.append('<hr>')
    $('#ollascomunes').append(olla)
  })

  // Items
  var items = $('.olla')
  items = [].slice.call(items)

  // Search input Filter
  $('#filter').on('input', () => {
    var filter = $('#filter').val().toLowerCase()
    items.forEach((item) => {
      var data = item.innerHTML.toLowerCase()
      var index = data.indexOf(filter)
      if(index > -1) {
        item.style.display = ''
      } else {
        item.style.display = 'none'
      }
    })
  })

})
$('#agregar').submit((e) => {
  e.preventDefault()
  var data = {}
  $.each($('#agregar').serializeArray(), function(_, item) {
    data[item.name] = item.value.trim() || null
  })
  console.log(data)
  $.post(url, data)
    .done(() => { 
      console.log('success') 
      window.location.reload(true)
    })
    .fail((error) => { 
      console.log(error)
      alert('ERROR ' + error.status + ': ' + error.statusText)
    })
})
$('.hora').attr('type', 'text')
$('.fecha').attr('type', 'text')
$('.hora').focus((e) => {
  e.target.type = 'time'
  $('.hora').click()
})
$('.fecha').focus((e) => {
  e.target.type = 'date'
  $('.fecha').click()
})
var region = (numero) => {
  var region
  switch  (numero) {
    case 'I':
      region = 'Región de Tarapacá'
      break;
    case 'II':
      region = 'Región de Antofagasta'
      break;
    case 'III':
      region = 'Región de Atacama'
      break;
    case 'IV':
      region = 'Región de Coquimbo'
      break;
    case 'V':
      region = 'Región de Valparaíso'
      break;
    case 'VI':
      region = 'Región del Libertador General Bernardo O\'Higgins'
      break;
    case 'VII':
      region = 'Región del Maule'
      break;
    case 'VIII':
      region = 'Región de Concepción'
      break;
    case 'IX':
      region = 'Región de la Araucanía'
      break;
    case 'X':
      region = 'Región de Los Lagos'
      break;
    case 'XI':
      region = 'Región de Aysén del General Carlos Ibañez del Campo'
      break;
    case 'XII':
      region = 'Región de Magallanes y de la Antártica Chilena'
      break;
    case 'XIII':
      region = 'Región Metropolitana'
      break;
    case 'XIV':
      region = 'Región de Los Ríos'
      break;
    case 'XV':
      region = 'Región de Arica y Parinacota'
      break;
    case 'XVI':
      region = 'Región del Ñuble'
      break;
  }
  return region
}
